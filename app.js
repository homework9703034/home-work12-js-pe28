"use strict";


const btnWrapper = document.querySelector('.btn-wrapper');
const buttons = btnWrapper.querySelectorAll('button');  

document.addEventListener('keydown', (event) => {
    const key = event.key 
    buttons.forEach((button) => {
        if (button.textContent === key || button.textContent === key.toUpperCase()) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    });
});